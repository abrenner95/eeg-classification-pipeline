%% Specifiy conditions
trainList = 1:2717; % List of training samples
trainDataFolder = 'TUHtrain_60s';
trainClassesFolder = 'TUHtrain';
testList = 1:276; % List of test samples
testDataFolder = 'TUHeval_60s';
testClassesFolder = 'TUHeval';
fileName = '\p';


%% Specify classes for classification and preprocessing
mgr = MainManager();
storeClassifier = false; % Should the classifier (e.g. trained NN) be stored?
cmgr = ClassificationManager(storeClassifier);
for n = 1:1
    cmgr.addClassifier(NeuralNetwork(strcat('nn', num2str(10)), [1])); % [15 15]
end
res = Resampling(250); % Resampling(250) -> resample all records to 250 Hz
fil = Filtering(1, 50); % Filtering(1,50) -> band-pass filter 1 Hz to 50 Hz
nor = Normalization3(); % Normalization per record
win = Windowing2(10, 5); % Windowing(10,5) -> 10 sec window with 5 sec overlap
%art = ArtifactDetection(100);
wav = WaveletExtraction(6, 'sym7'); % WaveletExtraction(6,'sym7')
pro = ProgressBar(10); % ProgressBar(10) -> plot progress in 10 steps


%% Run training samples
numFiles = length(trainList);
all_featvec = cell(numFiles,1);
numTrainEpochs = zeros(numFiles,1);

for n = 1:numFiles
    eeg = mgr.loadEEG(strcat(trainDataFolder, fileName, num2str(trainList(n))));
    % eeg = mgr.selectChannel(eeg, 'o1', 21); % Select channel 'o1' from 21 channels
    
    eeg = res.run(eeg);
    eeg = fil.run(eeg);
    eeg = nor.run(eeg);
    
    eeg = win.run(eeg);
    %eeg = art.run(eeg);
    eeg = wav.run(eeg);
    
    featvec = mgr.eeg2featvec(eeg);
    
    all_featvec{n} = featvec;
    numTrainEpochs(n) = eeg.numEpochs;
    
    [newState, toPrint] = pro.checkProgress(n,numFiles);
    if newState
        fprintf(toPrint);
    end
end

trainClassesEEG = mgr.loadMAT(strcat(trainClassesFolder, '\classes.mat'));
trainClassesEEG = trainClassesEEG(trainList,:);

[trainData, trainClassesEpochs, ~] = mgr.prepareTrainData(all_featvec, trainClassesEEG);

pro.reset();


%% Run test samples
numFiles = length(testList);
all_featvec = cell(numFiles,1);
numTestEpochs = zeros(numFiles,1);

for n = 1:numFiles
    eeg = mgr.loadEEG(strcat(testDataFolder, fileName, num2str(testList(n))));
    % eeg = mgr.selectChannel(eeg, 'o1', 21); % Select channel 'o1' from 21 channels

    
    eeg = res.run(eeg);
    eeg = fil.run(eeg);
    eeg = nor.run(eeg);
    
    eeg = win.run(eeg);
    %eeg = art.run(eeg);
    eeg = wav.run(eeg);
    
    featvec = mgr.eeg2featvec(eeg);
    
    all_featvec{n} = featvec;
    numTestEpochs(n) = eeg.numEpochs;
    
    [newState, toPrint] = pro.checkProgress(n,numFiles);
    if newState
        fprintf(toPrint);
    end
end

testClassesEEG = mgr.loadMAT(strcat(testClassesFolder, '\classes.mat'));
testClassesEEG = testClassesEEG(testList,:);

[testData, testClassesEpochs, ~] = mgr.prepareTrainData(all_featvec, testClassesEEG);


%% Run classification
resultsEpochs = cmgr.classifyEpochs(testData, testClassesEpochs, trainData, trainClassesEpochs);

[results, score] = cmgr.vote(resultsEpochs, numTestEpochs, testClassesEEG, 0.5);