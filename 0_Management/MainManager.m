classdef MainManager < handle
    %MAINMANAGER Manages functions for loading and selecting EEG data
    
    properties (Access = private)
        % Channel names according the 10/20 System
        channelnames21 = {'fp1', 'fp2', 'f3', 'f4', 'c3', 'c4', 'p3', 'p4', 'o1', 'o2', 'f7', 'f8', 't3', 't4', 't5', 't6', 'a1', 'a2', 'fz', 'cz', 'pz'};
        channelnames65 = {'Fp1','Fp2','F3','F4','C3','C4','P3','P4','O1','O2','F7','F8','T7','T8','P7','P8','Fz','Cz','Pz','Oz','FC1','FC2','CP1','CP2','FC5','FC6','CP5','CP6','TP9','TP10','POz','F1','F2','C1','C2','P1','P2','AF3','AF4','FC3','FC4','CP3','CP4','PO3','PO4','F5','F6','C5','C6','P5','P6','AF7','AF8','FT7','FT8','TP7','TP8','PO7','PO8','FT9','FT10','FPz','CPz','AFz','FCz'};
        
        tcp1 = {'fp1', 'f7', 't3', 't5', 'fp2', 'f8', 't4', 't6', 'a1', 't3', 'c3', 'cz', 'c4', 't4', 'fp1', 'f3', 'c3', 'p3', 'fp2', 'f4', 'c4', 'p4'};
        tcp2 = {'fz', 't3', 't5', 'o1', 'f8', 't4', 't6', 'o2', 't3', 'c3', 'cz', 'c4', 't4', 'a2', 'f3', 'c3', 'p3', 'o1', 'f4', 'c4', 'p4', 'o2'};
    end
    
    methods (Access = public)
        function obj = MainManager()
            obj = obj@handle();
        end
        
        function data = loadMAT(~, filepath)
            %LOADMAT Loads data from filepath and stores it in variable
            % data
            %
            % filepath = 'C:\eegdata.mat'
            buf = struct2cell(load(filepath));
            data = buf{1};
        end
        
        function eeg = loadEEG(obj, filepath)
            %LOADEEG Loads eeg data from filepath and stores it in variable
            % eeg
            %
            % filepath = 'C:\eegdata.mat'
            eeg = obj.loadMAT(filepath);
            % Convert eeg.data to double
            eeg.data = cellfun(@double, eeg.data, 'UniformOutput', false);
        end
        
        function counter = loadEDF(~, parFolder, fileList, desLengthInSec, desNumOfChannels, desFolder, desName, counter)
            %LOADEDF Loads EEG data from .edf format and stores the raw signal data
            % into .mat files containing signal data for each selected channel and the
            % sampling rate.
            %
            % parFolder = 'C:\user\parFolder'
            % Folder where the fileLists are stored
            %
            % fileList
            % List of all filepaths
            %
            % desLengthInSec
            % Cuts eeg records to the desired length in seconds
            %
            % desNumOfChannels
            % Cuts eeg records to the desired number of channels
            %
            % desFolder
            % Folder where the data will be stored
            % 
            % desName
            % Name prefix for the data to be stored
            %
            % counter
            % Name suffix for the data to be stored. Incrementally
            % increases.
            
            % Read all file paths
            fileID = fopen(strcat(parFolder,fileList), 'r');
            formatSpec = '%s';
            filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
            fclose(fileID);
            % Default: counter = 1
            if nargin < 8
                counter = 1;
            end
            % Iterate over filepaths
            for n = 1:length(filePaths{1})
                % Select current filepath
                curPath = filePaths{1}{n};
                % Load current EEG
                curEEG = pop_biosig(curPath, 'channels', 1:desNumOfChannels, 'importevent', 'off', 'importannot', 'off');
                
                % Check length of EEG record
                curLength = size(curEEG.data,2);
                desLength = desLengthInSec * curEEG.srate;
                if curLength < desLength
                    continue % If current EEG record is too short, continue with next one
                end
                % Select only desired channels
                curData = cell(1,desNumOfChannels);
                for c = 1:desNumOfChannels
                   curData{1,c} = curEEG.data(c,1:desLength);
                end
                eeg = struct('srate', curEEG.srate, 'numEpochs', 1, 'numChannels', desNumOfChannels);
                eeg.data = curData;
                % Save eeg struct
                save(strcat(desFolder,desName,int2str(counter)), 'eeg');
                counter = counter + 1;
            end
        end
        
        function counter = loadVHDR(~, parFolder, fileList, desLengthInSec, desNumOfChannels, desFolder, desName, counter)
            %LOADEDF Loads EEG data from .edf format and stores the raw signal data
            % into .mat files containing signal data for each selected channel and the
            % sampling rate.
            %
            % parFolder = 'C:\user\parFolder'
            % Folder where the fileLists are stored
            %
            % fileList
            % List of all filepaths
            %
            % desLengthInSec
            % Cuts eeg records to the desired length in seconds
            %
            % desNumOfChannels
            % Cuts eeg records to the desired number of channels
            %
            % desFolder
            % Folder where the data will be stored
            % 
            % desName
            % Name prefix for the data to be stored
            %
            % counter
            % Name suffix for the data to be stored. Incrementally
            % increases.
            
            % Read all file paths
            fileID = fopen(strcat(parFolder,fileList), 'r');
            formatSpec = '%s';
            filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
            fclose(fileID);
            
            if nargin < 8
                counter = 1;
            end
            for n = 1:length(filePaths{1})
                curPath = filePaths{1}{n};
                % Load current EEG
                curEEG = pop_loadbv(parFolder, curPath);
                
                % Check length of EEG record
                curLength = size(curEEG.data,2);
                desLength = desLengthInSec * curEEG.srate;
                if curLength < desLength
                    warning('File too short.\n Counter: %d. File: %s%s', counter, parFolder, curPath);
                    continue % If current EEG record is too short, continue with next one
                end
                
                curData = cell(1,desNumOfChannels);
                for c = 1:desNumOfChannels
                   curData{1,c} = curEEG.data(c,1:desLength);
                end
                eeg = struct('srate', curEEG.srate, 'numEpochs', 1, 'numChannels', desNumOfChannels);
                eeg.data = curData;
                
                save(strcat(desFolder,desName,int2str(counter)), 'eeg');
                counter = counter + 1;
            end
        end
        
        function counter = cutEEG(obj, parFolder, fileName, numFiles, desLengthInSec, desOffsetInSec, desNumOfChannels, desFolder, desName, counter)
            %CUTEEG Loads eeg records in .mat format and cuts them
            % according to the desired length or number of Channels. 
            % The resulting new eeg records are stored in .mat format.
            
            if nargin < 10
                counter = 1;
            end
            for n = 1:numFiles
                eeg = obj.loadEEG(strcat(parFolder, fileName, num2str(n)));
                
                % Check length of EEG record
                curLength = size(eeg.data{1,1},2);
                desLength = desLengthInSec * eeg.srate;
                desOffset = desOffsetInSec * eeg.srate;
                if curLength < desLength + desOffset
                    continue % If current EEG record is too short, continue with next one
                end
                
                eeg.data = eeg.data(1,1:desNumOfChannels);
                eeg.data = cellfun(@(x) x(:,desOffset+1:desOffset+desLength), eeg.data, 'UniformOutput', false);
                
                
                save(strcat(desFolder,desName,int2str(counter)), 'eeg');
                counter = counter + 1;
            end
        end
        
        function featvec = eeg2featvec(~, eeg)
            %EEG2FEATVEC returns the eeg.data in one matrix
            %
            
            featvec = cell2mat(eeg.data);
        end
        
        function eeg = selectChannel(obj, eeg, channelRange, numChannels)
            %SELECTCHANNEL Filters the eeg struct for the specified
            % channels
            
            % Default: numChannels = 65
            if nargin < 4
                numChannels = 65;
            end
            % Check if channel range is given be a string, e.g. {'t5', 'o1'}
            if iscell(channelRange) || ischar(channelRange)
                if numChannels == 65
                    idx = ismember(obj.channelnames65, channelRange);
                else
                    idx = ismember(obj.channelnames21, channelRange);
                end
                eeg.data = eeg.data(idx);
                if iscell(channelRange)
                    eeg.numChannels = length(channelRange);
                else
                    eeg.numChannels = 1;
                end
            else
                % Channel range given numerically, e.g. [1:20]
                eeg.data = eeg.data(channelRange);
                eeg.numChannels = length(channelRange);
            end
        end
        
        function eeg = selectTime(~, eeg, timeRange)
            if length(timerange) == 1
                eeg.data = eeg.data(1:timeRange);
            else
                eeg.data = eeg.data(timeRange);
            end
        end
        
        function [trainData, trainClassesExt, epochsPerEEG] = prepareTrainData(~, all_featvec, trainClasses)
            trainData = cell2mat(all_featvec);
            epochsPerEEG = zeros(size(trainClasses, 1), 1);
            
            % Extend classes according to the epochs
            tmpTrainClasses = zeros(size(trainData, 1), size(trainClasses, 2));
            c = 1;
            for n = 1:length(trainClasses)
                % Number of epochs of the current Record
                numCls = size(all_featvec{n,1}, 1);
                epochsPerEEG(n, 1) = numCls;
                % Copy the record's class for all epochs
                cls = repmat(trainClasses(n,:), [numCls, 1]);
                tmpTrainClasses(c:c+numCls-1, :) = cls;
                c = c + numCls;
            end
            trainClassesExt = tmpTrainClasses;
        end
        
    end
    
    methods (Access = private)
        
    end
    
end

