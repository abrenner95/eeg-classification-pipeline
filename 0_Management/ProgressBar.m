classdef ProgressBar < handle
    %PROGRESSBAR
    
    properties (Access = private)
        state;
        numOfSteps;
        reachedMax;
    end
    
    methods (Access = public)
        % Constructor
        function obj = ProgressBar(numOfSteps)
            obj = obj@handle();
            obj.numOfSteps = numOfSteps;
            obj.state = 0;
            obj.reachedMax = false;
        end
        
        function [newState, toPrint] = checkProgress(obj, curIteration, maxIteration)
            curState = (curIteration / maxIteration)*obj.numOfSteps;
            if curState >= (obj.state + 1)
                obj.state = floor(curState);
                dotOut = ['[' repmat('=',1,obj.state) repmat(' ',1,obj.numOfSteps-obj.state) ']\n'];
                newState = true;
                toPrint = dotOut;
            else
                newState = false;
                toPrint = '';
            end
        end
        
        function reset(obj)
            obj.state = 0;
            obj.reachedMax = 0;
        end
        
    end
    
    methods (Access = private)
    end
    
end