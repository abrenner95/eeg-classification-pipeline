mgr = MainManager();
fileName = 'p';
desLengthInSec = 60;
desNumOfChannels = 21;
desName = 'p';

numFiles = 2717;
parFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHtrain\';
desFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHtrain_60s\';

counter = 1;
counterTrain = mgr.cutEEG(parFolder, fileName, numFiles, desLengthInSec, desOffsetInSec, desNumOfChannels, desFolder, desName, counter);

numFiles = 276;
parFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHeval\';
desFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHeval_60s\';

counter = 1;
counterEval = mgr.cutEEG(parFolder, fileName, numFiles, desLengthInSec, desOffsetInSec, desNumOfChannels, desFolder, desName, counter);