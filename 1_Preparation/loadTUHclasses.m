parFolder = 'D:\tuh_data\v2.0.0\';
fileLists = {'train_abnormal.txt', 'train_normal.txt', 'eval_abnormal.txt', 'eval_normal.txt'};

fileID = fopen(strcat(parFolder,fileLists{1}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes1 = repmat([1,0], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{2}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes2 = repmat([0,1], length(filePaths{1}), 1);

classes = [classes1; classes2];

desFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHtrain\';
save(strcat(desFolder,'classes'), 'classes');


fileID = fopen(strcat(parFolder,fileLists{3}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes1 = repmat([1,0], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{4}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes2 = repmat([0,1], length(filePaths{1}), 1);

classes = [classes1; classes2];

desFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHeval\';
save(strcat(desFolder,'classes'), 'classes');