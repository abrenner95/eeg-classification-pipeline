% loadEDF(parFolder, fileList, desLengthInSec, desNumOfChannels, desFolder, desName)
mgr = MainManager();
parFolder = 'D:\tuh_data\v2.0.0\';
fileLists = {'train_abnormal.txt', 'train_normal.txt', 'eval_abnormal.txt', 'eval_normal.txt'};
desLengthInSec = 480;
desNumOfChannels = 21;
desFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHtrain\';
desName = 'p';

counter = 1;
for n = 1:2 %:length(fileLists)
    counter = mgr.loadEDF(parFolder, fileLists{n}, desLengthInSec, desNumOfChannels, desFolder, desName, counter);
end

desFolder = 'C:\Users\alex-\Documents\MATLAB\abnormal-normal\TUHeval\';

counter = 1;
for n = 3:4 %:length(fileLists)
    counter = mgr.loadEDF(parFolder, fileLists{n}, desLengthInSec, desNumOfChannels, desFolder, desName, counter);
end