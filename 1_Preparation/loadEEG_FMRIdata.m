% loadEDF(parFolder, fileList, desLengthInSec, desNumOfChannels, desFolder, desName)
mgr = MainManager();
parFolder = 'G:\HiWi\eeg-fmri_data\EEGfMRI data Jana\Export\';
fileLists = {'ssri.txt', 'atd.txt', 'plc.txt'};
desLengthInSec = 480;
desNumOfChannels = 65;
desFolder = 'C:\Users\Mitarbeiter\Documents\MATLAB\abnormal-normal\EEG_FMRI\';
desName = 'p';

counter = 1;
for n = 1:length(fileLists)
    counter = mgr.loadVHDR(parFolder, fileLists{n}, desLengthInSec, desNumOfChannels, desFolder, desName, counter);
end