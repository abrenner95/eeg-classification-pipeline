saveTo = 'SSRIvsPLC\';
numFilesSSRI = 27;
numFilesATD = 28;
numFilesPLC =  27;

numFilesALL = 82;
numFiles = numFilesSSRI + numFilesPLC;

k = 5;

cvFolds = crossvalind('Kfold', numFiles, k);

trainFolds = cell(1, k);
testFolds = cell(1, k);

for curFold = 1:k
    testIdx = (cvFolds == curFold);
    trainIdx = ~testIdx;
    
    trainIdxNew = false(numFilesALL,1);
    trainIdxNew(1:numFilesSSRI) = trainIdx(1:numFilesSSRI);
    trainIdxNew(numFilesSSRI+numFilesATD+1:numFilesALL) = trainIdx(numFilesSSRI+1:numFiles);
    
    testIdxNew = false(numFilesALL,1);
    testIdxNew(1:numFilesSSRI) = testIdx(1:numFilesSSRI);
    testIdxNew(numFilesSSRI+numFilesATD+1:numFilesALL) = testIdx(numFilesSSRI+1:numFiles);
    
    trainFolds{curFold} = find(trainIdxNew); % Remove find() for binary labeling
    testFolds{curFold} = find(testIdxNew);
end

save(strcat(saveTo, 'trainFolds.mat'), 'trainFolds');
save(strcat(saveTo, 'testFolds.mat'), 'testFolds');