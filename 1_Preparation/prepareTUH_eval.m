ext1 = 'A';
ext2 = 'N';
saveTo = 'tuh_eval\';
n = 1;

buf = struct2cell(load(strcat('data_TUH_normalized\EEGtrain', ext1, int2str(n))));
data1 = buf{1};

buf = struct2cell(load(strcat('data_TUH_normalized\EEGtrain', ext2, int2str(n))));
data2 = buf{1};

classes1 = [ones(length(data1),1), zeros(length(data1),1)];
classes2 = [zeros(length(data2),1), ones(length(data2),1)];

classesT = [classes1; classes2];


buf = struct2cell(load(strcat('data_TUH_normalized\EEGeval', ext1, int2str(n))));
data1 = buf{1};

buf = struct2cell(load(strcat('data_TUH_normalized\EEGeval', ext2, int2str(n))));
data2 = buf{1};

classes1 = [ones(length(data1),1), zeros(length(data1),1)];
classes2 = [zeros(length(data2),1), ones(length(data2),1)];

classesE = [classes1; classes2];


classes = [classesT; classesE];