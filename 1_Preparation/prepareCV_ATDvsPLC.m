saveTo = 'ATDvsPLC\';
numFilesSSRI = 27;
numFilesATD = 28;
numFilesPLC =  27;

numFilesALL = 82;
numFiles = numFilesATD + numFilesPLC;

k = 5;

cvFolds = crossvalind('Kfold', numFiles, k);

trainFolds = cell(1, k);
testFolds = cell(1, k);

for curFold = 1:k
    testIdx = (cvFolds == curFold);
    trainIdx = ~testIdx;
    
    trainIdxNew = false(numFilesALL,1);
    trainIdxNew(numFilesSSRI+1:numFilesSSRI+numFilesATD) = trainIdx(1:numFilesATD);
    trainIdxNew(numFilesSSRI+numFilesATD+1:numFilesALL) = trainIdx(numFilesATD+1:numFiles);
    
    testIdxNew = false(numFilesALL,1);
    testIdxNew(numFilesSSRI+1:numFilesSSRI+numFilesATD) = testIdx(1:numFilesATD);
    testIdxNew(numFilesSSRI+numFilesATD+1:numFilesALL) = testIdx(numFilesATD+1:numFiles);
    
    trainFolds{curFold} = find(trainIdxNew); % Remove find() for binary labeling
    testFolds{curFold} = find(testIdxNew);
end

save(strcat(saveTo, 'trainFolds.mat'), 'trainFolds');
save(strcat(saveTo, 'testFolds.mat'), 'testFolds');