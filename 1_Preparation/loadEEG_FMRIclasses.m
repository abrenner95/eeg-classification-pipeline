parFolder = 'G:\HiWi\eeg-fmri_data\EEGfMRI data Jana\Export\';
fileLists = {'ssri.txt', 'atd.txt', 'plc.txt'};

%%
fileID = fopen(strcat(parFolder,fileLists{1}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes1 = repmat([1,0], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{2}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes2 = repmat([0,1], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{3}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes3 = repmat([0,1], length(filePaths{1}), 1);

classes_SSRIvsREST = [classes1; classes2; classes3];

desFolder = 'C:\Users\Mitarbeiter\Documents\MATLAB\abnormal-normal\EEG_FMRI\';
save(strcat(desFolder,'classes_SSRIvsREST'), 'classes_SSRIvsREST');


%%
fileID = fopen(strcat(parFolder,fileLists{1}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes1 = repmat([0,1], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{2}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes2 = repmat([1,0], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{3}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes3 = repmat([0,1], length(filePaths{1}), 1);

classes_ATDvsREST = [classes1; classes2; classes3];

desFolder = 'C:\Users\Mitarbeiter\Documents\MATLAB\abnormal-normal\EEG_FMRI\';
save(strcat(desFolder,'classes_ATDvsREST'), 'classes_ATDvsREST');


%%
fileID = fopen(strcat(parFolder,fileLists{1}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes1 = repmat([0,1], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{2}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes2 = repmat([0,1], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{3}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes3 = repmat([1,0], length(filePaths{1}), 1);

classes_PLCvsREST = [classes1; classes2; classes3];

desFolder = 'C:\Users\Mitarbeiter\Documents\MATLAB\abnormal-normal\EEG_FMRI\';
save(strcat(desFolder,'classes_PLCvsREST'), 'classes_PLCvsREST');


%%
fileID = fopen(strcat(parFolder,fileLists{1}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes1 = repmat([1,0,0], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{2}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes2 = repmat([0,1,0], length(filePaths{1}), 1);

fileID = fopen(strcat(parFolder,fileLists{3}), 'r');
formatSpec = '%s';
filePaths = textscan(fileID, formatSpec, 'Delimiter', '\n');
fclose(fileID);
classes3 = repmat([0,0,1], length(filePaths{1}), 1);

classes_SSRIvsATDvsPLC = [classes1; classes2; classes3];

desFolder = 'C:\Users\Mitarbeiter\Documents\MATLAB\abnormal-normal\EEG_FMRI\';
save(strcat(desFolder,'classes_SSRIvsATDvsPLC'), 'classes_SSRIvsATDvsPLC');