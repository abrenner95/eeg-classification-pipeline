saveTo = 'CV_TUH;'; %'CV_TUH_small\';
numFiles = 2717; % 276

k = 5;

cvFolds = crossvalind('Kfold', numFiles, k);

trainFolds = cell(1, k);
testFolds = cell(1, k);

for curFold = 1:k
    testIdx = (cvFolds == curFold);
    trainIdx = ~testIdx  ;
    
    trainFolds{curFold} = find(trainIdx); % Remove find() for binary labeling
    testFolds{curFold} = find(testIdx);
end

save(strcat(saveTo, 'trainFolds.mat'), 'trainFolds');
save(strcat(saveTo, 'testFolds.mat'), 'testFolds');