# EEG-classification-pipeline

The pipeline can be used for automatized abnormal EEG detection. The repository provides scripts for training and testing the classification-pipeline on the [TUH Abnormal EEG Corpus](https://www.isip.piconepress.com/projects/tuh_eeg/html/downloads.shtml). The original purpose of the program is to detect abnormal EEG samples. The code can also be applied to a different dataset.

## Getting Started

Clone the repository and copy all files to your project folder. Open the project folder in MATLAB. Add the enumerated folders starting with a number to the MATLAB path. 

### Prerequisites

* MATLAB (last tested: MATLAB 2019a)
* Deep Learning Toolbox
* Wavelet Toolbox
* [EEGLAB](https://sccn.ucsd.edu/eeglab/downloadtoolbox.php/download.php) (last tested: eeglab2019_0)


### Getting the data

Download the [TUH Abnormal EEG Corpus](https://www.isip.piconepress.com/projects/tuh_eeg/html/downloads.shtml). If not given by the dataset, create .txt file listing all paths to the .edf files. For Windows for example [grepWin](https://tools.stefankueng.com/grepWin.html) can be used to search for *.edf files. The format should be as follows:
```	
C:\TUHdata\v2.0.0\edf\eval\abnormal\01_tcp_ar\007\00000768\s003_2012_04_06\00000768_s003_t000.edf
C:\TUHdata\v2.0.0\edf\eval\abnormal\01_tcp_ar\011\00001154\s007_2012_07_25\00001154_s007_t000.edf
C:\TUHdata\v2.0.0\edf\eval\abnormal\01_tcp_ar\012\00001217\s002_2012_09_17\00001217_s002_t000.edf
...
```

Once the data is downloaded and the .txt file(s) referencing all .edf files is/are ready, open the folder '1_Preparation' in the project folder.

The script **loadTUHdata.m** is used to load the EEG data from the EDF files listed in the .txt files. The scripts stores the relevant information including the raw signal data in .mat format in a destination folder of your choice. All following train / test and other scripts are accessing the EEG data in this .mat format. The script has the option to cut the data to a desired length and select the desired channels. Thus, the needed storage can be reduced.

The script **loadTUHclasses.m** is used to store the class information for the TUH data in .mat format.

## Running the tests

Once the data is loaded and stored in .mat format, the test scripts (starting with test_) can be executed. An example test script is **test_TUH_eval.m**. The test performs the following actions:

* Specifiyng the data for training and evaluation
* Specifiyng the pre-processing and classification methods
* Loading the training data and performing pre-processing on the data
* Training the classifier with the pre-processed training feature vectors
* Loading the evaluation data and performing pre-processing on the data
* Performing classification on the pre-processeed evaluation feature vectorts
* Performing voting to determine the accuracy/error rate of the classification per EEG sample
