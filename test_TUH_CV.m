%% Specify folds
load('CV_TUH/trainfolds');
load('CV_TUH/testFolds');
numFolds = length(trainFolds);

fileList = 1:2717; % List of all files
dataFolder = 'TUHtrain_60s';
classesFolder = 'TUHtrain';
fileName = '\p';


%% Specify classes for classification and preprocessing
mgr = MainManager();
storeClassifier = true; % Should the classifier (e.g. trained NN) be stored?
cmgr = ClassificationManager(storeClassifier);
for n = 1:1
    cmgr.addClassifier(NeuralNetwork(strcat('nn', num2str(1)), [1])); % [15 15]
end
res = Resampling(250); % Resampling(250) -> resample all records to 250 Hz
fil = Filtering(1, 50); % Filtering(1,50) -> band-pass filter 1 Hz to 50 Hz
nor = Normalization3(); % Normalization per record
win = Windowing2(10, 5); % Windowing(10,5) -> 10 sec window with 5 sec overlap
%art = ArtifactDetection(100);
wav = WaveletExtraction(6, 'sym7'); % WaveletExtraction(6,'sym7')
pro = ProgressBar(10); % ProgressBar(10) -> plot progress in 10 steps


%% Run training samples
numFiles = length(fileList);
all_featvec = cell(numFiles,1);
numTrainEpochs = zeros(numFiles,1);
score_all = zeros(1,numFolds);

for n = 1:numFiles
    eeg = mgr.loadEEG(strcat(dataFolder, fileName, num2str(fileList(n))));
    % eeg = mgr.selectChannel(eeg, 'o1', 21); % Select channel 'o1' from 21 channels
    
    eeg = res.run(eeg);
    eeg = fil.run(eeg);
    eeg = nor.run(eeg);
    
    eeg = win.run(eeg);
    %eeg = art.run(eeg);
    eeg = wav.run(eeg);
    
    featvec = mgr.eeg2featvec(eeg);
    
    all_featvec{n} = featvec;
    numTrainEpochs(n) = eeg.numEpochs;
    
    [newState, toPrint] = pro.checkProgress(n,numFiles);
    if newState
        fprintf(toPrint);
    end
end

pro.reset();

for k = 1:numFolds
    %% Specifiy conditions
    trainList = trainFolds{1,k}; % List of training samples
    testList = testFolds{1,k}; % List of test samples

    trainClassesEEG = mgr.loadMAT(strcat(classesFolder, '\classes.mat'));
    trainClassesEEG = trainClassesEEG(trainList,:);

    [trainData, trainClassesEpochs, ~] = mgr.prepareTrainData(all_featvec(trainList,:), trainClassesEEG);

    testClassesEEG = mgr.loadMAT(strcat(classesFolder, '\classes.mat'));
    testClassesEEG = testClassesEEG(testList,:);

    [testData, testClassesEpochs, ~] = mgr.prepareTrainData(all_featvec(testList,:), testClassesEEG);

    numTestEpochs = numTrainEpochs(testList,:);



    %% Run classification
    resultsEpochs = cmgr.classifyEpochs(testData, testClassesEpochs, trainData, trainClassesEpochs);

    [results, score_all(1,k)] = cmgr.vote(resultsEpochs, numTestEpochs, testClassesEEG, 0.5);
end

score = mean(score_all);