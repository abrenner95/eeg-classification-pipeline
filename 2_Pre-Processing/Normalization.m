classdef Normalization < handle
    %NORMALIZE per record
    
    properties (Access = private)
    end
    
    
    methods (Access = public)
        function obj = Normalization()
            obj = obj@handle();
        end
        
        function eeg = run(~, eeg)
            meanVal = mean(cellfun(@(x) mean(abs(x)), eeg.data));
            eeg.data = cellfun(@(x) x / meanVal, eeg.data, 'UniformOutput', false);
        end
    end
    
    
    methods (Access = private)
        
    end
end