classdef Filtering < handle
    %FILTERING Performs band-pass filtering on EEG records
    %   low: the high-pass cut-off
    %   high: low-pass cut-off
    
    properties (Access = private)
        low;
        high;
        type;
    end
    
    
    methods (Access = public)
        function obj = Filtering(low, high)
            obj = obj@handle();
            if nargin == 2
                obj.low = low;
                obj.high = high;
            elseif nargin == 1
                obj.low = low;
                obj.high = [];
            else
               obj.low = 1;
               obj.high = 60;
            end
        end
        
        function eeg = run(obj, eeg)
            %FILTER Performs band-pass filtering on the given records 
            % according to the Filter's low and high values.
            %
            % FILTER(obj, eegs)

            order = 2;
            srate = eeg.srate;
            if obj.low == 0 && obj.high > 0
                [b, a] = butter(order, obj.high/(srate/2), 'low');
            elseif obj.low > 0 && obj.high == 0
                [b, a] = butter(order, obj.low/(srate/2), 'high');
            else
                [b, a] = butter(order, [obj.low/(srate/2), obj.high/(srate/2)]);
            end
            eeg.data = cellfun(@(x) filter(b, a, x), eeg.data, 'UniformOutput', false);
            % fprintf('Filtered EEG data [%d - %d Hz]\n', obj.low, obj.high);
        end
    end
    
    
    methods (Access = private)
        
    end
end

