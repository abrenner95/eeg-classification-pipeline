classdef ArtifactDetection < handle
    %ARTIFACTDETECTION Checks wether the given EEG data exceed a threshold.
    %   Includes a threshold and a function for automatically deleting data
    %   segments exceeding the given threshold.
    
    properties (Access = private)
        threshold;
        quantity;
    end
    
    methods (Access = public)
        function obj = ArtifactDetection(threshold, quantity)
            obj = obj@handle();
            if nargin == 2
                obj.threshold = threshold;
                obj.quantity = quantity;
            elseif nargin == 1
                obj.threshold = threshold;
                obj.quantity = 0.5;
            else
                % Default threshold
                obj.threshold = 100;
                obj.quantity = 0.5;
            end
        end
        
        function eeg = run(obj, eeg)
            artifactListAll = cellfun(@(x) obj.detectArtifacts(x), eeg.data, 'UniformOutput', false);
            artifactListNum = sum(cell2mat(artifactListAll), 2);
            quantityThresh = eeg.numChannels * obj.quantity; %%% TODO
            artifactList = artifactListNum >= quantityThresh;
            eeg.numEpochs = sum(~artifactList);
            eeg.data = cellfun(@(x) x(~artifactList,:), eeg.data,'UniformOutput',false);
        end
           
    end
    
    methods (Access = private)
        function artifactList = detectArtifacts(obj, data)
            % Test wether the given data epoch exceeds the threshold
            
            dataLength = size(data,1);
            artifactList = zeros(dataLength,1);
            if obj.threshold > 0
                for n = 1:dataLength
                    if (max(abs(data(n,:))) >= obj.threshold || max(abs(data(n,:))) == 0)
                        artifactList(n,1) = 1;
                    end
                end
            else
                for n = 1:dataLength
                    if (max(abs(data(n,:))) == 0)
                        artifactList(n,1) = 1;
                    end
                end
            end
        end
        
    end
    
end

