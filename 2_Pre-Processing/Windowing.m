classdef Windowing < handle
    %WINDOWING Performs windowing on the given data
    %   Includes a fucntion for performing windowing according to a given
    %   window size and window overlap
    
    properties (Access = private)
        winSizeInSec;
        winOverlapInSec;
    end
    
    methods (Access = public)
        function obj = Windowing(winSize, winOverlap)
            obj = obj@handle();
            if nargin == 2
                obj.winSizeInSec = winSize;
                obj.winOverlapInSec = winOverlap;
            elseif nargin == 1
                obj.winSizeInSec = winSize;
                obj.winOverlapInSec = 0;
            else
                obj.winSizeInSec = 20;
                obj.winOverlapInSec = 10;
            end
        end
        
        function eeg = run(obj, eeg)
            % PERFORMWINDOWING Performs windowing on the given records 
            % according to the Windowing objects's winSize and winOverlap.
            % Each data row in a records object will be splitted in
            % multiple data rows (epochs).
            %
            % PERFORMWINDOWING(obj, eegs)
            winSize = obj.winSizeInSec * eeg.srate;
            winOverlap = obj.winOverlapInSec * eeg.srate;
            displacement = winSize - winOverlap;
            numFeatureVectors = floor((size(eeg.data{1,1}, 2) - winSize) / displacement) + 1;
            eeg.numEpochs = numFeatureVectors;
            eeg.data = cellfun(@(x) obj.windowDataRow(x, winSize, winOverlap), eeg.data, 'UniformOutput', false);
        end
    end
    
    methods (Access = private)      
        
        function features = windowDataRow(~, datarow, winSize, winOverlap)
            % Performs windowing on a single data row
            displacement = winSize - winOverlap;
            numFeatureVectors = floor((size(datarow, 2) - winSize) / displacement) + 1;
            features = zeros(numFeatureVectors, winSize);
            for n = 0:numFeatureVectors-1
                startPos = 1 + n * displacement;
                endPos = startPos + winSize - 1;
                newData = datarow(1, startPos:endPos);
                features(n+1, :) = newData;
            end
        end
    end
    
end

