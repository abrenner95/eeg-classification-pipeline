classdef Resampling < handle
    %RESAMPLING Performs resampling on EEG records
    
    properties (Access = private)
        srate;
    end
    
    
    methods (Access = public)
        function obj = Resampling(samplingRate)
            obj = obj@handle();
            if nargin == 1
               obj.srate = samplingRate; 
            else
               obj.srate = 128; % Default
            end
        end
        
        function eeg = run(obj, eeg)
            %RESAMPLE Performs resampling on the given record according 
            % to the Resampling object's sampling rate.
            %
            % RESAMPLE(obj, eegs)
            
            eeg.data = cellfun(@(x) resample(x', obj.srate, eeg.srate)', eeg.data, 'UniformOutput', false);
            eeg.srate = obj.srate;
            % fprintf('Resampled EEG data [%d Hz]\n', obj.srate);
        end
    end
    
    
    methods (Access = private)

    end
end

