classdef Normalization3 < handle
    %NORMALIZE per record
    
    properties (Access = private)
    end
    
    
    methods (Access = public)
        function obj = Normalization3()
            obj = obj@handle();
        end
        
        function eeg = run(~, eeg)
            data = cell2mat(eeg.data);
            dataNew = normalize(data, 'zscore');
            dataLength = length(eeg.data{1});
            offset = 0;
            for n = 1:eeg.numChannels
                eeg.data{n} = dataNew(1+offset:offset+dataLength);
                offset = offset + dataLength;
            end
        end
    end
    
    
    methods (Access = private)
        
    end
end