classdef DWT < handle
    %WAVELETEXTRACTION Performs a wavelet decomposition and extracts features
    %   Includes a level filter  and feature extraction function 
    
    properties (Access = private)
        name;
        level;
        filter;
    end
    
    methods (Access = public)
        function obj = DWT(level, filter)
            obj.level = level;
            obj.filter = filter;
        end
        
        function eeg = run(obj, eeg)
            eeg.data = cellfun(@(x) obj.extractFeatures(x), eeg.data, 'UniformOutput', false);
        end
        
    end
    
    methods (Access = private)
        function features = extractFeatures(obj, data)
            %EXTRACTFEATURES Performs feature extraction on the given data
            % with the objects wavelet filter and level. The calculated
            % features are: Max, Min, Mean and Std of the wavelet
            % coefficients, wavelet entropy and relative wavelet energy.
            %
            % features = extractFeatures(obj, data)
            
            features = zeros(size(data, 1), 6*(obj.level+1));
            for curData = 1:size(data, 1)
                features(curData, :) = obj.createFeatureVector(data(curData, :));
            end
        end
        
        
        function featureVector = createFeatureVector(obj, data)
           % perform wavelet decomposition
           [c,l] = wavedec(data, obj.level, obj.filter);
           [ea, ed] = wenergy(c,l);
           energy = [ea, ed];
           
           offset = 6;
           featureVector = zeros(1, offset*(obj.level+1));
           coffset = 1;
           soffset = 1;
           for n = 1:obj.level
               coeff = detcoef(c,l,n);
               entropy = wentropy(coeff, 'log energy');
               rwe = energy(1, n);
               featureVector(1, soffset:soffset+offset-1) = [max(coeff), min(coeff), mean(coeff), std(coeff), entropy, rwe];
               soffset = soffset + offset;
               coffset = coffset + l(n);
           end
           n = obj.level + 1;
           coeff = appcoef(c,l,obj.filter);
           entropy = wentropy(coeff, 'log energy');
           rwe = energy(1, n);
           featureVector(1, soffset:soffset+offset-1) = [max(coeff), min(coeff), mean(coeff), std(coeff), entropy, rwe];
        end
    end
    
end

